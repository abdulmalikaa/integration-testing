/**
 * @param {string} hex
 * @returns {string}
 */

const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
     /**
      * @param {number} red
      * @param {number} green
      * @param {number} blue
      * @returns {string}
      */
    rgbToHex: (red, green, blue) => { 
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString (16);
        const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex;
    }
};