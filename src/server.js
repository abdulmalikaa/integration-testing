const express = require("express");
const converter = require('./converter');
const app = express();
const PORT = 3000;

app.get('/', (req, res) => res.send("Welcome!"));

app.get('/hex-to-rgb', (req, res) => {
    const red = req.query.r;
    const green = req.query.g;
    const blue = req.query.b;
    const hex = converter.HexTorgb(red, green, blue);
    res.send(rgb);
});

console.log("NODE_ENV: " + process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(PORT, () => console.log("Server listening..."));
}

console.log("NODE_ENV: " + process.env.NODE_ENV);