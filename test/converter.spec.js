// TDO - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("Hex to RGB conversions", () => {
        it("converts the basic colors", () => {
            const redHex = converter.HexTorgb('ff0000');
            expect(redHex).to.equal(255, 0, 0);
            
            const blueHex = converter.HexTorgb('#0000ff');
            expect(blueHex).to.equal(0, 0, 255);

            const Hex = converter.rgbToHex('#0000ff');
            expect(blueHex).to.equal(0, 0, 255);            
        });
    });
});